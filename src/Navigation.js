import React, { Component } from 'react';
import './Navigation.css';
class Navigation extends Component {

    render() { 


        const NavArr = ['Home', 'About','Services','Portfolio','Contact']
        const NavLinks = NavArr.map((navlink, index) => 
            <li key={index}>
                <a href={'#'+{navlink}} > {navlink} </a>
            </li>
        )

        return ( 
            <React.Fragment>
            <nav>
                <h1 className='logo'>{this.props.LogoTitle}</h1>
                <ul>
                    {NavLinks}
                </ul>
            </nav>
            </React.Fragment>
         );
    }
}
 
export default Navigation;