import React, {Component} from 'react';
import Background from './img/Pencil-Background-Image.jpg';
import './Header.css';
const HeaderStyles = {
    backgroundImage: `url(${Background})`,
    height: '40vh',
    backgroundSize: 'cover'
}

class Header extends Component {

    render() { 
        return (
            <header style={HeaderStyles}>
                <h1>{this.props.HeaderTitle}</h1>
                <p>
                    <em>I am programmer as backend developer</em>
                </p>
                <a href='#button'>{this.props.HeaderButton}</a>
            </header>
        );
    }
}
 
export default Header;