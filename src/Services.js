import React,{Component} from 'react';
import './Services.css'

// const  IconObjs = [
//         {
//             icon: <ion-icon name="phone-portrait"></ion-icon>,
//             title: 'Responsive',
//             description: 'Looks great on any screen size!'
//         },
//         {
//             icon: <ion-icon name="brush"></ion-icon>,
//             title: 'Redesigned',
//             description: 'Freshly redesigned for styles'
//         },
//         {
//             icon: <ion-icon name="thumbs-up"></ion-icon>,
//             title: 'Favorited',
//             description: 'Millions of users  start this!'
//         },
//         {
//             icon:  <ion-icon name="help"></ion-icon>,
//             title: 'Question',
//             description: 'I mustache you a question..'
//         },
//     ];

   // class Icons extends Component {
   //     render() { 
   //         return ( 
   //          <div>
   //              <span>
   //                  {this.props.icon}
   //              </span>
   //              <h4>{this.props.title}</h4>
   //              <p>{this.props.desc}</p>
   //          </div>
   //          );
   //     }
   // }
    

class Services extends Component {

    constructor(props) {
        super(props);
        this.state = {
          icons: [
                {
                    icon: <ion-icon name="phone-portrait"></ion-icon>,
                    title: 'Responsive',
                    description: 'Looks great on any screen size!'
                },
                {
                    icon: <ion-icon name="brush"></ion-icon>,
                    title: 'Redesigned',
                    description: 'Freshly redesigned for styles'
                },
                {
                    icon: <ion-icon name="thumbs-up"></ion-icon>,
                    title: 'Favorited',
                    description: 'Millions of users  start this!'
                },
                {
                    icon:  <ion-icon name="help"></ion-icon>,
                    title: 'Question',
                    description: 'I mustache you a question..'
                },
            ]
        }
    }

    render() { 
        const { icons } = this.state
        console.log({icons})
        return ( 
            <div className="services">
                <h3>services </h3>
                <h2>What We Offer</h2>

                <div className='row'>
                    { icons.map((icon, index) => 
                        <div key={index}>
                            <span>
                                {icon.icon}
                            </span>
                            <h4>{icon.title}</h4>
                            <p>{icon.description}</p>
                        </div>
                        ) }
                </div>
            </div>
        );
    }

}
 
export default Services;