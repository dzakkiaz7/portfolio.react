import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Navigation from './Navigation';
import Header from './Header';
import Services from './Services';
import * as serviceWorker from './serviceWorker';

class App extends React.Component {
    render() { 
        return ( 
            <React.Fragment>
                <Navigation LogoTitle='React Project' />
                <Header HeaderTitle='Dzakkiaz Portfolio' HeaderButton='Find Out More' />
                <Services />
            </React.Fragment>
         );
    }
}

ReactDOM.render(<App/>, document.getElementById('root'));

serviceWorker.unregister();
